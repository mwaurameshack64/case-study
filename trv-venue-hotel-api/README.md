![sample app image](https://drive.google.com/file/d/1pWsWIS_8iox5R2ad6wNvAAR7slvU0-GJ/view?usp=sharing)

## Getting Started
1. cd into the api directory
    + `$ cd trv-venue-hotel-api`

3. install json-server to help run the api
    + `$ npm i json-server`

2. Run the backend API server in a different terminal instance
    + `$ json-server db.json`

