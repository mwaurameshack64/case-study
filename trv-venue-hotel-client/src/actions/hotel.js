import axios from "axios";
import * as dotenv from "dotenv";

dotenv.config()

export const allHotels = async () =>
  await axios.get(`${process.env.REACT_APP_API}/hotels`);

export const read = async (hotelId) =>
  await axios.get(`${process.env.REACT_APP_API}/hotels/${hotelId}`);

