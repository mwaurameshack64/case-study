import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';

// Add internationalization to support multiple languages in the future
import './i18n';
import reportWebVitals from "./reportWebVitals";


ReactDOM.render(
  <React.StrictMode>
        <App />
  </React.StrictMode>,
  document.getElementById("root")
);

reportWebVitals();
