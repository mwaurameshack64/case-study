
import Title from '../components/Title';
import RoomsList from '../components/RoomsList'
import DatePicker from '../components/DatePicker'

const RoomsContainer = (props) => {
    const featuredRooms = props.rooms.map((room) => {
        return <RoomsList key={room.id} room={room} />;
    })
    return (
        <>
        <section className="featured-rooms"> 
            <DatePicker/>
            <Title title="featured Rooms"/>
            <div className="featured-rooms-center">
                {featuredRooms}
            </div>
        </section>
        </>
    );
  };
  
  export default RoomsContainer;