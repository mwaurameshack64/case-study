import React from 'react';
import '../assets/styles/sass/index.scss';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Future internationalization
import { withNamespaces } from 'react-i18next';

import Navbar from '../components/Navbar';
import Home from '../pages/Home';
import Rooms from '../pages/Rooms';
import ConfirmCheckout from '../pages/Checkout';
import PrivateRoute from '../components/PrivateRoute';
import Login from '../pages/Login';


const App = () => {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/login" component={Login} />
        <PrivateRoute exact path="/hotels/:hotelId" component={Rooms} />
        <PrivateRoute exact path="/hotel/room-checkout/:roomId" component={ConfirmCheckout} />
      </Switch>
    </BrowserRouter>
  );
};

export default withNamespaces()(App);
