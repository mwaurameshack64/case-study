import {useEffect} from 'react';
import Cover from '../components/Cover';
import Banner from '../components/Banner';
import FeaturedHotels from '../components/FeaturedHotels';

const Home = () => {

  // Set Dummy data to be used for authentication and checkout page 
  let userName = 'John Doe';
  let email = 'j.doe@gmail.com';
  let cardNumber = 4447564829324;
  let expirationDate = "23/11";
  let cvv = 342;

  const authData = {
    "name":userName,
    "email":email,
    "cardNumber":cardNumber,
    "expirationDate":expirationDate,
    "cvv":cvv,

  }

  const setAuthDetails = () => {
    window.localStorage.setItem("auth", JSON.stringify(authData))
  }

  useEffect(() => {
    setAuthDetails()
  }, [])

  return (
    <>
      <Cover> 
        <Banner 
        title="luxurious hotels"
        subtitle="Executive experience at your convenience"
        >
        </Banner>
      </Cover>
      <FeaturedHotels />
    </>
  );
};

export default Home;