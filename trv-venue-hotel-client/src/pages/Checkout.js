import React, {useEffect, useState} from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import defaultImg from "../assets/images/room-1.jpeg";
import Banner from '../components/Banner';
import { Link } from 'react-router-dom';
import StyledCover from '../components/StyledCover';

const CheckOut = () => {
    const [auth, setAuth] = useState({})
    const history = useHistory()
    const location = useLocation()

    useEffect(() => {}, [location])

    useEffect(() => {
        const authDetails = window.localStorage.getItem('auth')
        setAuth(JSON.parse(authDetails))
    }, [])

    const handleClick = (e) => {
        e.preventDefault()
        alert('🥳🥳🥳🥳🥳🥳🥳Room Booked Successfully')
        history.push('/')
    }

    const Input = (props) => (
        <div className="input">
          <label>{props.label}</label>
          <div className="input-field">
            <input type={props.type} name={props.name} placeholder={props.name}/>
            <img src={props.imgSrc}/>
          </div>
        </div>
      );
      
      const Button = (props) => (
        <button onClick={props.onClick} className="checkout-btn" type="button">{props.text}</button>
      );
    return (
        <React.Fragment>
               <StyledCover img={defaultImg}>
                   <Banner title={`${location.state.name} Room`} >
                       <Link to='/' className="btn-primary">back to hotels</Link>
                   </Banner>
               </StyledCover>
               <section className="single-room">
                   <div className="single-room-info">
                       <article className="desc">
                           <h3>your payment details</h3>
                           <div className="checkout">
                            <div className="checkout-container">
                            <h3 className="heading-3">Credit card checkout</h3>
                            <Input label="Cardholder's Name" type="text" name={auth.name} />
                            <Input label="Card Number" type="number" name={auth.cardNumber} imgSrc="https://seeklogo.com/images/V/visa-logo-6F4057663D-seeklogo.com.png" />
                            <div className="row">
                                <div className="col">
                                <Input label="Expiration Date" name={auth.expirationDate} />
                                </div>
                                <div className="col">
                                <Input label="CVV" type="number" name={auth.cvv} />
                                </div>
                            </div>
                            <Button text="Place order" onClick={handleClick} />
                            </div>
                        </div>
                       </article>
                       <article className="info">
                           <h3>room info</h3>
                           <h6>room name : {location.state.name}</h6>
                           <h6>price in usd: ${location.state.price_in_usd} per night</h6>
                           <h6>max capacity : { location.state.max_occupancy > 1 ? `${location.state.max_occupancy} people` : `${location.state.max_occupancy} person` }</h6>
                           <h6>Rating : {location.state.rating}</h6>
                       </article>
                   </div>
               </section>
            </React.Fragment>
    );
};

export default CheckOut;
