import React, { useState, useEffect } from "react";
import Cover from '../components/Cover';
import Banner from '../components/Banner';
import { Link } from 'react-router-dom';
import RoomsContainer from '../containers/RoomsContainer'
import { read } from "../actions/hotel";


const Rooms = ({ match }) => {
    const [hotel, setHotel] = useState({});

    useEffect(() => {
        loadHotelRooms();
    }, []);

    const loadHotelRooms = async () => {
        let res = await read(match.params.hotelId);
        setHotel(res.data);
    };

    return (
        <>
        <Cover coverClass="roomsHero">
            <Banner 
            title={hotel.name}
            >
            <p>{hotel.description}</p>
            <Link to="/" className="btn-primary">return home</Link>
            </Banner>
        </Cover>
        {hotel.rooms && (
            <RoomsContainer rooms={hotel.rooms} />
        )
        }   
        </>
    );
};

export default Rooms;