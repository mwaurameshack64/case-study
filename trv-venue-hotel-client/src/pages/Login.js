import { useState } from "react";
import LoginForm from "../components/LoginForm";

const Login = ({ history }) => {
  const [email, setEmail] = useState("j.doe@gmail.com");
  const [password, setPassword] = useState("password123");

  const handleSubmit = async (e) => {
    e.preventDefault();

      // Some Dummy data as we do not have a backend in place yet
        let userName = 'John Doe';
        let email = 'j.doe@gmail.com';
        let cardNumber = 4447564829324;
        let expirationDate = "23/11";
        let cvv = 342;

        const authData = {
            "name":userName,
            "email":email,
            "cardNumber":cardNumber,
            "expirationDate":expirationDate,
            "cvv":cvv,

        }
        // save user and token to local storage
        window.localStorage.setItem("auth", JSON.stringify(authData));
        history.push("/");
  };

  return (
    <>
      <div className="container-fluid bg-secondary p-5 text-center">
        <h1>Login</h1>
      </div>

      <div className="container">
        <div className="row">
          <div className="col-md-6 offset-md-3">
            <LoginForm
              handleSubmit={handleSubmit}
              email={email}
              setEmail={setEmail}
              password={password}
              setPassword={setPassword}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Login;
