import { useState, useEffect } from 'react';
import Title from './Title';
import Hotel from './Hotel';
import { allHotels } from "../actions/hotel";

const FeaturedHotels = () => {
    const [hotels, setHotels] = useState([]);
  
    useEffect(() => {
      loadAllhotels();
    }, []);
  
    const loadAllhotels = async () => {
      let res = await allHotels();
      setHotels(res.data);
    };

    const featuredHotels = hotels.map((hotel) => {
        return <Hotel key={hotel.id} hotel={hotel} />;
    })
    return (
      <>
        <section className="featured-rooms"> 
            <Title title="featured hotels"/>
            <div className="featured-rooms-center">
                {featuredHotels}
            </div>
        </section>
      </>
    );
  };
  
  export default FeaturedHotels;