const LoginForm = ({
    handleSubmit,
    email,
    setEmail,
    password,
    setPassword,
  }) => (
    <form onSubmit={handleSubmit}>
            <h2>Welcome Back!</h2>
              <ul>
                <li>
                  <label for="email">Email Address:</label>
                  <input
                    id="email"
                    type="email"
                    className="form-control"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                </li>
                <li>
                  <label for="password">Password:</label>
                  <input
                    id="password"
                    type="password"
                    className="form-control"
                    placeholder="Enter password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                </li>
              </ul>
            <button disabled={!email || !password} className="btn btn-primary">
                Submit
            </button>
    </form>
  );
  export default LoginForm;
  