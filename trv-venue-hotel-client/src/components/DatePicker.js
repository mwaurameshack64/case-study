import React, { useState } from "react";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";

function CheckinDate() {
  const [checkInDate, setCheckInDate] = useState(null);
  const [checkOutDate, setCheckOutDate] = useState(null);

  const handleCheckInDate = (date) => {
    setCheckInDate(date);
    setCheckOutDate(null);
  };

  const handleCheckOutDate = (date) => {
    setCheckOutDate(date);
  };

  const addDays = (date, days) => {
    const copyDate = new Date(Number(date))
    copyDate.setDate(date.getDate() + days)
    return copyDate
  }

  const dateDifference = () => {

    const first_conference_day = new Date()
    // Set First Conference Day in the future
    // first_conference_day.setDate(first_conference_day.getDate() + 10);
    const last_conference_day = addDays(first_conference_day, 1);

    const conference_time_diff = first_conference_day.getTime() - last_conference_day.getTime()
    const conference_days_diff = conference_time_diff / (1000 * 3600 * 24)
    const checkin_time_diff = checkOutDate.getTime() - checkInDate.getTime()
    const checkin_days_diff = checkin_time_diff / (1000 * 3600 * 24);

    if (checkin_days_diff < conference_days_diff){
        return("You can only book a room for one Night for the duration of the conference")
    } else if (checkInDate > last_conference_day) {
        return("Checkin Date cannot be later than the last day of the conference")
    } else if (checkOutDate < first_conference_day) {
        return("Checkout Date cannot be before  the first day of the conference")
    } else {
        return(`Your room will be booked from ${moment(checkInDate).format("LL")} to
        ${moment(checkOutDate).format("LL")} `)
    }
  }

  return (
    <div className="date-picker">
      <div className="input-container">
        <div>
          <label className="section-title">Check-in</label>
          <DatePicker
            selected={checkInDate}
            minDate={new Date()}
            onChange={handleCheckInDate}
          />
        </div>
        <div>
          <label className="section-title">Check-out</label>
          <DatePicker
            selected={checkOutDate}
            minDate={checkInDate}
            onChange={handleCheckOutDate}
          />
        </div>
      </div>
      {checkInDate && checkOutDate && (
        <div className="summary">
          <p>
            {dateDifference()}.
          </p>
        </div>
      )}
    </div>
  );
}

export default CheckinDate;