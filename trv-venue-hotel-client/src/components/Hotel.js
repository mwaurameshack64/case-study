import React from 'react';
import defaultImg from "../assets/images/room-1.jpeg";
import { Link } from 'react-router-dom';
import StarRatings from 'react-star-ratings';


const Hotel = (props) => {
    const { id, name, price_category, rating, distance_to_venue, amenities, images } = props.hotel;
    return (
        <>
        <article className="room">
            <div className="img-container">
                <img src={images[0].image || defaultImg} alt="single room" />
                <div className="price-top">
                    <h6>{distance_to_venue}</h6>
                    <p>miles from venue</p>
                </div>
                <Link to={`/hotels/${id}`} className="btn-primary room-link">View Rooms</Link>
             </div>
             <p className="room-info">{name}</p>
             <p className="room-info">Price Category: {price_category}</p>
             <p><span className="rating-info">Rating:</span> 
                 <StarRatings
                rating={rating}
                starDimension="30px"
                starSpacing="15px"
                />
            </p>
             <div className="room-info"> Amenities</div>
             {amenities.split(', ').map((amenity, i) => (
                 <div key={i} className="room-info">{amenity}</div>
             ))}
        </article>
        </>
    );
};

export default Hotel;