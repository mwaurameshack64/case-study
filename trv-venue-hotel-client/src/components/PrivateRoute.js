import React, {useEffect, useState} from 'react';
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = ({ ...rest }) => {
    const [auth, setAuth] = useState({})
    useEffect(() => {
        const authDetails = window.localStorage.getItem('auth')
        setAuth(JSON.parse(authDetails))
    }, [])

    return auth ? <Route {...rest} /> : <Redirect to="/login" />;
};

export default PrivateRoute;
