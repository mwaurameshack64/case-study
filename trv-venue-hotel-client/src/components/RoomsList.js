import React from 'react';
import { useHistory} from 'react-router-dom';
import defaultImg from "../assets/images/room-1.jpeg";
import StarRatings from 'react-star-ratings';

const RoomsList = (props) => {
    const { id, name, description, max_occupancy, rating, price_in_usd, available } = props.room;
    const history = useHistory()

    const handleClick = (e) => {
        e.preventDefault()
        if(available === 'true'){
            history.push({
                pathname:`/hotel/room-checkout/${id}`,
                state:{
                    "name": name,
                    "description": description,
                    "price_in_usd": price_in_usd,
                    "max_occupancy": max_occupancy,
                    "rating": rating
                }
            })
        } else {
            alert("🥺 🥺 Oooh no this room isn't available for booking!!!")
        }

    }
    return (
        <>
        <article className="room">
            <div className="img-container">
                <img src={defaultImg} alt="single room" />
                <div className="price-top">
                    <h6>${price_in_usd}</h6>
                    <p>per night</p>
                </div>
                <button
                    onClick={handleClick}
                    className="btn-primary room-link"
                >
                    Book Room
                </button>
             </div>
             <p className="room-info">room name : {name} </p>
             <p className="room-info">our thoughts : {description} </p>
             <p><span className="rating-info">Rating:</span> 
                <StarRatings
                rating={rating}
                starDimension="30px"
                starSpacing="15px"
                />
            </p>
            <div className="room-info">
                <p> Maximum Occupancy: { max_occupancy > 1 ? `${max_occupancy} people` : `${max_occupancy} person` }</p>
                <p>Available: {available}</p>
            </div>
        </article>
        </>
    );
};

export default RoomsList;
