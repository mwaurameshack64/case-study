import React from 'react';
import defaultImg from "../assets/images/room-1.jpeg";
import { Link } from 'react-router-dom';


const Room = (props) => {
    const { id, name, price_category, distance_to_venue, images } = props.hotel;
    return (
        <>
        <article className="room">
            <div className="img-container">
                <img src={images[0] || defaultImg} alt="single room" />
                <div className="price-top">
                    <h6>{distance_to_venue}</h6>
                    <p>miles</p>
                </div>
                <Link to={`/hotels/${id}`} className="btn-primary room-link">View Rooms</Link>
             </div>
             <p className="room-info">{name} - Price Category: {price_category}</p>
        </article>
        </>
    );
};

export default Room;