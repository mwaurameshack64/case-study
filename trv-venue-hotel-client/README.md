**SetUp Instructions**
---
1. cd into client directory
    + `$ cd trv-venue-hotel-client`

2. install with [`npm`](https://www.npmjs.com/)
    + `$ npm install`

3. Run the App Locally 
    + `$ npm start`

4. Generate the App build
    + `$ npm run build`

## File structure
#### `trv-venue-hotel-client` - Holds the client application
- #### `public` - This holds all of our static files
- #### `src`
    - #### `assets` - This folder holds assets such as images, docs, and fonts.
    - #### `components` - This folder holds all of the different components that will make up some of the views.
    - #### `containers` - This folder holds App.js and RoomsContaine.js.
    - #### `locales` - This folder holds the language internationalization for different languages.
    - #### `pages` - This folder holds the main navigation pages for the application.
    - #### `i18n.js` - This file holds the configuration files for internationalization.
    - #### `index.js` - This is what renders the react app by rendering App.js, should not change
- #### `package.json` - Defines npm behaviors and packages for the client
#### `README.md` - This file!