#### _**IMPORTANT NOTE**_ - 
## Future Areas of impovement
1. Use Redux for state management.
    + `I did not use any state management library in the project but that is definitely a factor that I could improve on should we ever need to scale the application`

2. The project doesn't have a dashboard. The case study had stated that it was nice to have but I didn't include it. However, in the future we I can have the dashboard for      administrative purposes.

3. Login page could use some improvements in the future. Didn't give it too much thought as we are already providing a login user.
